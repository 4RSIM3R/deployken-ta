FROM composer:2.0.9 as preparation
WORKDIR /app
COPY .env.production .env
COPY . /app
RUN composer install

FROM php:7.4-apache
RUN apt-get update -y && apt-get install -y openssl zip unzip git
RUN apt-get install -y libxml2-dev libonig-dev vim
RUN docker-php-ext-install pdo mbstring mysqli pdo pdo_mysql mbstring
COPY --from=preparation /app .
COPY vhost.conf /etc/apache2/sites-available/000-default.conf
RUN chown -R www-data:www-data .
RUN a2enmod rewrite