<?php

namespace App\Http\Middleware;

use App\Http\Response\WebResponse;
use App\Models\Deployment;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Repository\Deployment\DeploymentRepository;
use Repository\Plan\PlanRepository;

class LimitMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // dd(Auth::user());
        $user = Auth::user();
        $userId = $user->id;
        $userPlanId = $user->plan_id;

        $plan = PlanRepository::countPlan($userPlanId);
        $planLimit = $plan->limit;

        $artifactCount = DeploymentRepository::countArtifact($userId);
        
        if ($artifactCount >= $planLimit) {
            return WebResponse::error('Whoops, you reach your deployment limits');
        }
        
        return $next($request);
    }
}
