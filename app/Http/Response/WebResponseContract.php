<?php

namespace App\Http\Response;

interface WebResponseContract
{

    public static function success($data, $message = '', $code = 200);

    public static function error($message, $code = 400);
}
