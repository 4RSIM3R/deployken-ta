<?php

namespace App\Http\Response;

use Illuminate\Http\JsonResponse;

class WebResponse implements WebResponseContract
{

    public static  function success($data, $message = '', $code = 200): JsonResponse
    {
        return new JsonResponse([
            'success' => true,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public static function error($message, $code = 400): JsonResponse
    {
        return new JsonResponse([
            'success' => false,
            'message' => $message,
        ], $code);
    }

}
