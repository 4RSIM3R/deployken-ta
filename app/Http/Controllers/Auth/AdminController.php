<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\JsonResponse;
use Repository\Auth\AuthRepository;

class AdminController extends Controller
{

    public function login(AuthRepository $repository, LoginRequest $request): JsonResponse
    {
        $request->validated();
        return $repository->loginAdmin($request->validationData());
    }

    public function register(AuthRepository $repository, RegisterRequest $request): JsonResponse
    {
        $request->validated();
        return $repository->registerAdmin($request->validationData());
    }

    public function logout(AuthRepository $repository): JsonResponse
    {
        return $repository->logout();
    }
}
