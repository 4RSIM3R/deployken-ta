<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\JsonResponse;
use Repository\Auth\AuthRepository;

class AuthController extends Controller
{


    public function login(LoginRequest $request, AuthRepository $repository): JsonResponse
    {
        $request->validated();
        return $repository->login($request->validationData());
    }

    public function register(RegisterRequest $request, AuthRepository $repository): JsonResponse
    {
        $request->validated();
        return $repository->register($request->validationData());
    }

    public function logout(AuthRepository $repository): JsonResponse
    {
        return $repository->logout();
    }
}
