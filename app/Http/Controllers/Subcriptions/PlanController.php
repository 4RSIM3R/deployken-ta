<?php

namespace App\Http\Controllers\Subcription;

use App\Http\Controllers\Controller;
use App\Http\Requests\PlanRequest;
use Illuminate\Http\JsonResponse;
use Repository\Subcription\SubcriptionRepository;

class PlanController extends Controller {

    public function all(SubcriptionRepository $repository): JsonResponse
    {
        return $repository->getAllPlan();
    }

    public function store(SubcriptionRepository $repository, PlanRequest $request)
    {
        $request->validated();
        return $repository->addPlan($request->validationData());
    }

}