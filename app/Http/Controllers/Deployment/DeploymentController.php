<?php

namespace App\Http\Controllers\Deployment;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeploymentRequest;
use App\Http\Response\WebResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Repository\Deployment\DeploymentRepository;

class DeploymentController extends Controller
{

    public function all(DeploymentRepository $repository, $username): JsonResponse
    {
        return $repository->getAllArtifact($username);
    }

    public function store(DeploymentRepository $repository, Request $request, $username): JsonResponse
    {

        $this->validate($request, [
            'artifact' => 'required'
        ]);

        $artifact = $request->file('artifact');
        $artifactName = time() . '-' . $artifact->getClientOriginalName();

        return $repository->handleArtifact($artifact, $artifactName, $username);
    }
}
