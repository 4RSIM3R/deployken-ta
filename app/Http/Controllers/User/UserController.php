<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\VerifyRequest;
use Illuminate\Http\JsonResponse;
use Repository\User\UserRepository;

class UserController extends Controller {

    public function all(UserRepository $respository): JsonResponse
    {
        return $respository->getAllUser();
    }

    public function verify(UserRepository $respository, VerifyRequest $request): JsonResponse
    {
        $request->validated();
        return $respository->verifyUser($request->validationData());
    }

    public function banned()
    {
        # code...
    }
}