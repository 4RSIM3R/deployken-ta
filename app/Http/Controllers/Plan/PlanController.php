<?php

namespace App\Http\Controllers\Plan;

use App\Http\Controllers\Controller;
use App\Http\Requests\PlanRequest;
use App\Http\Response\WebResponse;
use Illuminate\Http\JsonResponse;
use Repository\Plan\PlanRepository;

class PlanController extends Controller {

    public function all(PlanRepository $repository): JsonResponse
    {
        return $repository->getAllPlan();
    }

    public function store(PlanRequest $request, PlanRepository $repository): JsonResponse
    {
        $request->validated();
        return $repository->storePlan($request->validationData());
    }

    public function edit(PlanRequest $request, PlanRepository $repository, $id): JsonResponse
    {
        $request->validated();
        return $repository->editPlan($request->validationData(), $id);
    }

    public function delete(PlanRepository $repository, $id)
    {
        return $repository->deletePlan($id);
    }
    
}