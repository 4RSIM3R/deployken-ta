## About Deployken

Deployken is reliable beta-apps distribution with various way to distribute your beta-apps to client, or early customers.

## Notes For Devlopment

- This project currently using Repository Pattern, so dont forget to add `Repository` to `composer.json`
- And type this command : `composer dump-autoload` and done

## Notes For Containerization 

- Dont forget to update the image on `docker-compose` process :

    - `docker-compose pull`
    - `docker-compose up -d --remove-orphans`
    - `docker-compose image prune` optionally when you want to remove the orphan image

## Notes For Deployment

- Dont forget to run this followng command when you get fisrt deploy : 

    - Ensure you have log-on to `bash` of `webapp` container
    - You can do that with `docker exec -it webapp bash`
    - And run this following command
    - `php artisan migrate`
    - `php artisan permission:create-role admin` && `php artisan permission:create-role user`



