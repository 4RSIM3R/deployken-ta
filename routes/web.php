<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return 'Assalamualaikum, Deployken Monolith API';
});

Route::get('/host', function(){
    return 'Alhamdulillah, this app running @' . gethostname();
});

Route::get('/config', function(){
    return ini_get_all();
});


Route::get('/demo', function(){
    return "Ini Demo Bisa";
});