<?php

use App\Http\Controllers\Auth\AdminController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Deployment\DeploymentController;
use App\Http\Controllers\Plan\PlanController;
use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::get('deployments/{username}', [DeploymentController::class, 'all']);
    // Route::middleware('auth:api')->group(function () {
    //     Route::post('deployment/{username}', [DeploymentController::class, 'store']);
    // });
    Route::group(['middleware' => ['auth:api', 'limit']], function(){
        Route::post('deployment/{username}', [DeploymentController::class, 'store']);
    });
});

Route::prefix('v1')->group(function () {

    Route::prefix('admin')->group(function () {

        Route::post('login', [AdminController::class, 'login']);
        Route::post('register', [AdminController::class, 'register']);
        Route::middleware('auth:api')->group(function () {
            Route::post('logout');
        });
    });
});

Route::prefix('v1')->group(function () {

    Route::get('plans', [PlanController::class, 'all']);

    Route::group(['prefix' => 'manage', 'middleware' => ['auth:api', 'role:admin']], function () {
        Route::prefix('plan')->group(function () {
            Route::post('store', [PlanController::class, 'store']);
            Route::put('edit/{id}', [PlanController::class, 'edit']);
            Route::delete('delete/{id}', [PlanController::class, 'delete']);
        });

        Route::prefix('user')->group(function () {
            Route::get('all', [UserController::class, 'all']);
            Route::post('verify', [UserController::class, 'verify']);
        });

        Route::prefix('subcribe')->group(function () {
            Route::get('all');
        });
    });

});


Route::prefix('v1')->group(function () {

    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('register', [AuthController::class, 'register'])->name('register');
    Route::middleware('auth:api')->group(function () {
        Route::post('logout', [AuthController::class, 'logout'])->name('logout');
    });
});
