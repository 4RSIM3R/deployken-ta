<?php

namespace Repository\Deployment;

interface DeploymentRepositoryContract
{
    function handleArtifact($artifact, $artifactName, $username);
    function findIdByUsername($username);
    function getAllArtifact($username);
}
