<?php

namespace Repository\Deployment;

use App\Http\Response\WebResponse;
use App\Models\Deployment;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DeploymentRepository implements DeploymentRepositoryContract
{

    public function handleArtifact($artifact, $artifactName, $username): JsonResponse
    {
        $userId = $this->findIdByUsername($username)[0]->id;
        try {
            DB::beginTransaction();
            Storage::disk('public')->put($artifactName, file_get_contents($artifact));
            $artifactUrl = Storage::url($artifactName);
            Deployment::create([
                'user_id' => $userId,
                'url' => $artifactUrl
            ]);
            DB::commit();
            return WebResponse::success('', 'Alhamdulillah, New Artifcat Deployed');
        } catch (\Throwable $th) {
            DB::rollBack();
            return WebResponse::error($th->getMessage());
        }
    }

    public function getAllArtifact($username): JsonResponse
    {
        $user = $this->findIdByUsername($username);
        if ($user->count() > 0) {
            $userId = $user[0]->id;
            try {
                $artifacts = DB::table('deployments')->where('user_id', '=', $userId)->get();
                foreach ($artifacts as $key => $artifact) {
                    $artifactArray = json_decode(json_encode($artifacts[$key]), true);
                    $artifacts[$key] = array_merge($artifactArray, ['url' => stripslashes(env('APP_URL') . $artifacts[$key]->url)]);
                }
                $result = [
                    'profile' => $this->getUsernameProfile($username),
                    'artifact' => $artifacts
                ];
                return WebResponse::success($result, 'Alhamdulillah, List of artifact');
            } catch (\Throwable $th) {
                return WebResponse::error($th->getMessage());
            }
        }
        return WebResponse::success('', 'Astaghfirullah, Cannot Find That Username');
    }

    public static function countArtifact($userId)
    {
        return DB::table('deployments')->where('user_id', '=', $userId)->get()->count();
    }


    public function findIdByUsername($username)
    {
        return User::where('username', $username)->get(['id']);
    }

    public function getUsernameProfile($username) {
        return User::where('username', $username)->get();
    }
}
