<?php

namespace Repository\Auth;

use App\Http\Response\WebResponse;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class AuthRepository implements AuthRepositoryContract
{

    public function login(array $request): JsonResponse
    {
        try {
            $user = User::where('email', '=', $request['email'])->first();

            if (!$user || !Hash::check($request['password'], $user->password)) {
                return WebResponse::error('The provided credentials are incorrect.', 401);
            }

            $credential = array_merge($user->only('email'), ['password' => $request['password']]);

            if (!$token = auth()->guard('api')->attempt($credential)) {
                return WebResponse::error('Unauthorized', 401);
            } else if ($user->hasRole('admin')) {
                return WebResponse::error('Unauthorized Role', 401);
            } else if ($user->status == 'pending') {
                return WebResponse::error('Your account has not been confirmed please contact the customer support', 401);
            } 
            $this->updateToken($request['email'], $token);
            return WebResponse::success(['token' => $token, 'type' => 'Bearer', 'user' => $user]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return WebResponse::error($th->getMessage(), 401);
        }
    }

    public function register(array $request)
    {
        try {
            DB::beginTransaction();
            $data = array_merge($request, [
                'password' => bcrypt($request['password']),
                'username' => $request['username'],
                'token' => '-',
                'plan_id' => $request['plan_id']
            ]);
            $newUser = User::create($data);
            // dd($newUser);
            $newUser->assignRole('user');
            DB::commit();
            return WebResponse::success($newUser, 'Alhamdulillah New User Registered');
        } catch (\Throwable $th) {
            DB::rollBack();
            return WebResponse::error($th->getMessage());
        }
    }

    public function loginAdmin(array $request): JsonResponse
    {
        try {
            $user = User::where('email', '=', $request['email'])->first();

            if (!$user || !Hash::check($request['password'], $user->password)) {
                return WebResponse::error('The provided credentials are incorrect.', 401);
            }

            $credential = array_merge($user->only('email'), ['password' => $request['password']]);

            if (!$token = auth()->guard('api')->attempt($credential)) {
                return WebResponse::error('Unauthorized', 401);
            } else if ($user->hasRole('user')) {
                return WebResponse::error('Unauthorized Role');
            }
            $this->updateToken($request['email'], $token);
            return WebResponse::success(['token' => $token, 'type' => 'Bearer']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return WebResponse::error($th->getMessage(), 401);
        }
    }

    public function registerAdmin(array $request)
    {
        try {
            DB::beginTransaction();
            $data = array_merge($request, [
                'password' => bcrypt($request['password']),
                'username' => $request['username'],
                'token' => '-',
                'status' => 'verified'
            ]);
            // dd(Role::all());
            $newAdmin = User::create($data);
            $newAdmin->assignRole('admin');
            DB::commit();
            return WebResponse::success($newAdmin, 'Alhamdulillah New User Registered');
        } catch (\Throwable $th) {
            DB::rollBack();
            return WebResponse::error($th->getMessage());
        }
    }

    private function updateToken($email, $token)
    {
        DB::beginTransaction();
        User::where('email', $email)->update(['token' => $token]);
        DB::commit();
    }

    public function logout()
    {
        auth('api')->logout();
        return WebResponse::success('Alhamdulillah Logout');
    }
}
