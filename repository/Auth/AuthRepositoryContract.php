<?php

namespace Repository\Auth;

interface AuthRepositoryContract {

    function login(array $request);
    function register(array $request);
    function logout();

    function loginAdmin(array $request);
    function registerAdmin(array $request);
}