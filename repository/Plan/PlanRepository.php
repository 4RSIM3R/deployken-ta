<?php

namespace Repository\Plan;

use App\Http\Response\WebResponse;
use App\Models\Plan;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class PlanRepository implements PlanRepositoryContract
{

    public function getAllPlan(): JsonResponse
    {
        try {
            $plans = Plan::all();
            return WebResponse::success($plans, 'Alhamdulillah, list of all plans');
        } catch (\Throwable $th) {
            return WebResponse::error($th->getMessage());
        }
    }

    public function storePlan(array $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            Plan::create($request);
            DB::commit();
            return WebResponse::success($request, 'Alhamdulillah, new plan added');
        } catch (\Throwable $th) {
            DB::rollBack();
            return WebResponse::error($th->getMessage());
        }
    }

    public function editPlan(array $request, $id)
    {
        try {
            DB::beginTransaction();
            Plan::query()->findOrFail($id)->update($request);
            $newPlan = Plan::query()->findOrFail($id);
            DB::commit();
            return WebResponse::success($newPlan, 'Alhamdulillah, plan updated');
        } catch (\Throwable $th) {
            DB::rollBack();
            return WebResponse::error($th->getMessage());
        }
    }

    public function deletePlan($id)
    {
        try {
            DB::beginTransaction();
            Plan::destroy($id);
            DB::commit();
            return WebResponse::success('', 'Alhamdulillah, plan deleted');
        } catch (\Throwable $th) {
            DB::rollBack();
            return WebResponse::error($th->getMessage());
        }
    }

    public static function countPlan($userPlanId)
    {
        return Plan::where('id', '=', $userPlanId)->first();
    }
}
