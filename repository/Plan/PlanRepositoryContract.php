<?php

namespace Repository\Plan;

interface PlanRepositoryContract {
    function getAllPlan();
    function storePlan(array $request);
    function editPlan(array $request, $id);
    function deletePlan($id);
}