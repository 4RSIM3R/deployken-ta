<?php

namespace Repository\User;

use App\Http\Response\WebResponse;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserRepositoryContract
{

    public function getAllUser(): JsonResponse
    {
        try {
            $users = User::role('user')->get();
            return WebResponse::success($users, 'Alhamdulillah, list of all users');
        } catch (\Throwable $th) {
            return WebResponse::error($th->getMessage());
        }
    }

    public function verifyUser(array $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            User::where('email', $request['email'])->update(['status' => 'verified']);
            DB::commit();
            return WebResponse::success('', 'Alhamdulillah, user verified');
        } catch (\Throwable $th) {
            DB::rollBack();
            return WebResponse::error($th->getMessage());
        }
    }
}
