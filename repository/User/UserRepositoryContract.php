<?php

namespace Repository\User;

interface UserRepositoryContract {
    function getAllUser();
    function verifyUser(array $request);
}